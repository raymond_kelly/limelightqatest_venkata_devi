package com.interview;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Amazon {

	WebDriver driver;
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());
	List<ResultItem> results;

	@BeforeClass
	public void openBrowser() {
		driver = Util.getChromeDriver();
		results = new ArrayList<ResultItem>();
	}

	@Test(priority = 1)
	public void verifyTitle() {
		String url = "https://www.amazon.com/";
		driver.get(url);
		LOGGER.log(Level.INFO, driver.getTitle());
		Assert.assertEquals(Locators.config.get("AmazonTitle"), driver.getTitle());
		LOGGER.log(Level.INFO, "You are in Amazon.com");

	}

	@Test(priority = 2)
	public void verifySearchForIpadAir2() {
		driver.findElement(By.xpath(Locators.config.get("MainSearch"))).sendKeys("ipad air 2 case");
		driver.findElement(By.xpath(Locators.config.get("SearchButton"))).click();
		String ipadtext = driver.findElement(By.xpath(Locators.config.get("IpadAirText"))).getText();
		Assert.assertEquals(ipadtext, "\"ipad air 2 case\"");
		LOGGER.log(Level.INFO, "IPad Air Search is Successful");
	}

	@Test(priority = 3)
	public void verifyRefinePlasticSearchCase() {
		WebElement ele = driver.findElement(By.xpath(Locators.config.get("PlasticSearch")));
		ele.click();
		LOGGER.log(Level.INFO, "Refine search for plastic IPad Air case is Successful");
	}

	@Test(priority = 4)
	public void verifyRefinePriceRangeSearch() throws InterruptedException {
		driver.findElement(By.xpath(Locators.config.get("Min"))).sendKeys("20");
		driver.findElement(By.xpath(Locators.config.get("Max"))).sendKeys("100");
		List<WebElement> eles = driver.findElements(By.xpath(Locators.config.get("Go")));
		for (WebElement ele : eles) {
			String outerHTML = ele.getAttribute("outerHTML");
			if (outerHTML.indexOf("Go") != -1) {
				ele.click();
			}

		}
		LOGGER.log(Level.INFO, "Refine search for Price on $20-$100 is Successful");

	}

	private List<ResultItem> getResults() {
		int noOfResults = 5;
		String xpathForPrice = Locators.config.get("ResultPrice");
		String xpathForPriceAlt = Locators.config.get("ResultPriceAlternative");
		String xpathForName = Locators.config.get("ResultName");
		String xpathForScore = Locators.config.get("ResultScore");
		List<ResultItem> results = new ArrayList<ResultItem>();
		for (int i = 0; i < noOfResults; i++) {
			String priceXpath = xpathForPrice.replaceFirst("XXXXX", "result_" + i);
			double price;
			try {
				price = Double.parseDouble(driver.findElement(By.xpath(priceXpath)).getText());
			} catch (NoSuchElementException ne) {
				String xpathForPriceAltXpath = xpathForPriceAlt.replaceFirst("XXXXX", "result_" + i);
				price = Double.parseDouble(driver.findElement(By.xpath(xpathForPriceAltXpath)).getText().substring(1));
			}
			String nameXpath = xpathForName.replaceFirst("YYYYY", "result_" + i);
			String name = driver.findElement(By.xpath(nameXpath)).getText();
			String scoreXpath = xpathForScore.replaceFirst("ZZZZZ", "result_" + i);
			double ratingValue = 0;
			try {
				String ratingString = driver.findElement(By.xpath(scoreXpath)).getAttribute("innerHTML");
				ratingValue = Double.parseDouble(ratingString.substring(0, ratingString.indexOf("out of")));
			} catch (NoSuchElementException ne) {
			}
			results.add(new ResultItem(price, name, ratingValue));
		}
		return results;
	}

	@Test(priority = 5)
	public void verifyResults() {
		this.results = getResults();
		for (ResultItem item : results) {
			LOGGER.log(Level.INFO, "****");
			LOGGER.log(Level.INFO, "Name = " + item.getName());
			LOGGER.log(Level.INFO, "Price = " + item.getPrice());
			LOGGER.log(Level.INFO, "Rating = " + item.getRating());
			LOGGER.log(Level.INFO, "****");
		}
	}

	@Test(priority = 6)
	public void verifyPrice() {
		for (ResultItem item : this.results) {
			Assert.assertTrue(item.getPrice() > 20 && item.getPrice() < 100);
		}
	}

	@Test(priority = 7)
	public void sortByPrice() {

		this.results.sort(new Comparator<ResultItem>() {
			@Override
			public int compare(ResultItem o1, ResultItem o2) {
				if (o1.getPrice() > o2.getPrice())
					return 1;
				else if (o1.getPrice() == o2.getPrice())
					return 0;
				else
					return -1;
			}
		});

		LOGGER.log(Level.INFO, this.results.toString());

	}

	@Test(priority = 8)
	public void sortByScore() {

		this.results.sort(new Comparator<ResultItem>() {
			@Override
			public int compare(ResultItem o1, ResultItem o2) {
				if (o1.getRating() > o2.getRating())
					return 1;
				else if (o1.getRating() == o2.getRating())
					return 0;
				else
					return -1;

			}
		});

		LOGGER.log(Level.INFO, this.results.toString());
	}

	@Test(priority = 8)
	public void verifySortByPriceAndAssert() {

		double[] expectedPrices = new double[5];
		int index = 0;
		for (ResultItem item : this.results) {
			expectedPrices[index++] = item.getPrice();
		}
		Arrays.sort(expectedPrices);

		double[] sortedPrices = new double[5];
		this.results.sort(new Comparator<ResultItem>() {
			@Override
			public int compare(ResultItem o1, ResultItem o2) {
				if (o1.getPrice() > o2.getPrice())
					return 1;
				else if (o1.getPrice() == o2.getPrice())
					return 0;
				else
					return -1;
			}
		});

		index = 0;
		for (ResultItem item : this.results) {
			sortedPrices[index++] = item.getPrice();
		}
		Assert.assertEquals(sortedPrices, expectedPrices);
	}

	@Test(priority = 9)
	public void recommendProduct() {

		this.results.sort(new Comparator<ResultItem>() {
			@Override
			public int compare(ResultItem o1, ResultItem o2) {
				if (o1.getRating() > o2.getRating())
					return 1;
				else if (o1.getRating() == o2.getRating()) {
					if (o1.getPrice() > o2.getPrice())
						return 1;
					else if (o1.getPrice() == o2.getPrice())
						return 0;
					else
						return -1;
				} else
					return -1;

			}
		});
		LOGGER.log(Level.INFO, "Recommended product name = " + this.results.get(4).getName());
		LOGGER.log(Level.INFO, "Recommended product rating = " + this.results.get(4).getRating());
		LOGGER.log(Level.INFO, "Recommended product price = " + this.results.get(4).getPrice());
	}

	@AfterClass
	public void closing() {
		driver.close();
		System.exit(0);
	}

}
