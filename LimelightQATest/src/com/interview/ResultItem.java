package com.interview;

public class ResultItem {
	private double price;
	private String name;
	private double rating;
	
	public ResultItem(double price, String name,double rating) {
		this.price = price;
		this.name = name;
		this.rating = rating;
	}
	
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}
	
	public String toString(){
		return price+" - "+rating;
	}

}
