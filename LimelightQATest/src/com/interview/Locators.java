package com.interview;

import java.util.HashMap;
import java.util.Map;

public class Locators {

	public static Map <String, String> config;
	static{
		config=new HashMap<String,String>();
		//content
		config.put("AmazonTitle", "Amazon.com: Online Shopping for Electronics, Apparel, Computers, Books, DVDs & more");
		
		// Locator paths
		config.put("MainSearch","//*[@id='twotabsearchtextbox']");
		config.put("SearchButton", "//*[@id='nav-search']/form/div[2]/div/input");
		config.put("IpadAirText", "//*[@id='bcKwText']/span");
		config.put("PlasticSearch", "//*[@id='leftNavContainer']/ul[4]/div/li[1]/span/span/div/label/input");
		config.put("Min", "//*[@id='low-price']");
		config.put("Max", "//*[@id='high-price']");
		config.put("Go", "//*[contains(@id,'a-autoid')]/span/input");
		config.put("SearchMinMax", "//*[@id='s-result-count']/span/a[7]");
		config.put("ResultPrice", "//*[@id='XXXXX']/div/div[4]/div[1]/a/span/span/span");
		config.put("ResultPriceAlternative", "//*[@id='XXXXX']/div/div[4]/div/a/span[2]");
		config.put("ResultName", "//*[@id='YYYYY']/div/div[3]/div[1]/a/h2");
		config.put("ResultScore", "//*[@id='ZZZZZ']/div/div[6]/span/span/a/i[1]/span");
		config.put("ResultRating", "//*[@id='aaaaa']/div/div/div/div[1]/span");
	}
}
