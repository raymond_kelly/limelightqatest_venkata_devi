package com.interview;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Util {
	static WebDriver driver = null;

	public static WebDriver getChromeDriver() {
		System.setProperty("webdriver.chrome.driver", "src/chromedriver.exe");
		return new ChromeDriver();
	}

}
